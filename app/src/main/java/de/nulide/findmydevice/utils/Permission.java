package de.nulide.findmydevice.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.widget.Toast;

import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.FragmentActivity;

import com.permissionx.guolindev.PermissionX;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import de.nulide.findmydevice.R;
import de.nulide.findmydevice.receiver.DeviceAdminReceiver;
import de.nulide.findmydevice.services.ThirdPartyAccessService;
import rikka.shizuku.Shizuku;

public class Permission {
    public enum PermissionTypes {
        SMS(
            List.of(
                Manifest.permission.SEND_SMS,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS),
            true,
            true
        ),
        CONTACTS(
            List.of(
                Manifest.permission.READ_CONTACTS),
            true,
            true
        ),
        FOREGROUND_LOCATION(
            List.of(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION),
            true,
            true
        ),
        @SuppressLint("InlinedApi")
        BACKGROUND_LOCATION(
            List.of(
                Manifest.permission.ACCESS_BACKGROUND_LOCATION),
            true,
            false
        ),
        DO_NOT_DISTURB(
            List.of(),
            false,
            false
        ),
        DEVICE_ADMIN(
            List.of(),
            false,
            false
        ),
        OVERLAY(
            List.of(),
            false,
            false
        ),
        WRITE_SECURE_SETTINGS(
            List.of(
                Manifest.permission.WRITE_SECURE_SETTINGS),
            true,
            false
        ),
        READ_NOTIFICATIONS(
            List.of(),
            false,
            false
        ),
        CAMERA(
            List.of(
                Manifest.permission.CAMERA),
            true,
            true
        ),
        BATTERY_OPTIMIZATIONS(
            List.of(),
            false,
            false
        ),
        @SuppressLint("InlinedApi")
        POST_NOTIFICATIONS(
            List.of(
                Manifest.permission.POST_NOTIFICATIONS),
            false,
            true
        );

        private final List<String> permissions;
        private final boolean isEasyToCheck;
        private final boolean isEasyToRequest;

        PermissionTypes(List<String> permissions, boolean isEasyToCheck, boolean isEasyToRequest) {
            this.permissions = permissions;
            this.isEasyToCheck = isEasyToCheck;
            this.isEasyToRequest = isEasyToRequest;
        }

        public boolean isGranted(Context context) {
            if (this == BACKGROUND_LOCATION && Build.VERSION.SDK_INT < Build.VERSION_CODES.Q)
                return true;

            if (isEasyToCheck)
                return permissions.stream().allMatch(p -> PermissionX.isGranted(context, p));
            else {
                switch (this) {
                    case DO_NOT_DISTURB -> {
                        return ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE))
                            .isNotificationPolicyAccessGranted();
                    }
                    case DEVICE_ADMIN -> {
                        return ((DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE))
                            .isAdminActive(new ComponentName(context, DeviceAdminReceiver.class));
                    }
                    case OVERLAY -> {
                        return Settings.canDrawOverlays(context);
                    }
                    case READ_NOTIFICATIONS -> {
                        String enabledListeners = Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
                        return enabledListeners != null && enabledListeners.contains(
                            new ComponentName(context, ThirdPartyAccessService.class).flattenToString()
                        );
                    }
                    case BATTERY_OPTIMIZATIONS -> {
                        return ((PowerManager) context.getSystemService(Context.POWER_SERVICE))
                            .isIgnoringBatteryOptimizations(context.getPackageName());
                    }
                    case POST_NOTIFICATIONS -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                            return PermissionX.isGranted(context, Manifest.permission.POST_NOTIFICATIONS) &&
                                NotificationManagerCompat.from(context).areNotificationsEnabled();
                        else
                            return NotificationManagerCompat.from(context).areNotificationsEnabled();
                    }
                    default -> { return false; }
                }
            }
        }

        @SuppressLint("BatteryLife")
        public void request(Context context) {
            if (isEasyToRequest)
                PermissionX.init((FragmentActivity) context)
                    .permissions(permissions)
                    .onForwardToSettings((scope, deniedList) ->
                        Utils.startAppSettingsActivityWithToast(context,
                            "Please, grant selected permission manually"))
                    .request(null);
            else {
                switch (this) {
                    case BACKGROUND_LOCATION -> Utils.startAppSettingsActivityWithToast(
                        context, "Please, grant selected permission manually"
                    );
                    case DO_NOT_DISTURB -> context.startActivity(
                        new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS)
                    );
                    case DEVICE_ADMIN -> context.startActivity(
                        new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
                            .putExtra(
                                DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                                new ComponentName(context, DeviceAdminReceiver.class))
                    );
                    case OVERLAY -> context.startActivity(
                        new Intent(
                            Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + context.getPackageName()))
                    );
                    case WRITE_SECURE_SETTINGS -> {/* Can be granted only via root or shizuku */}
                    case READ_NOTIFICATIONS -> context.startActivity(
                        new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS)
                    );
                    case BATTERY_OPTIMIZATIONS -> context.startActivity(
                        new Intent(
                            Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                            Uri.parse("package:" + context.getPackageName()))
                    );
                }
            }
        }
    }

    private static final int PERM_SMS_ID = 61341;
    private static final int PERM_GPS_ID = 61342;
    private static final int PERM_CONTACT_ID = 61343;
    private static final int PERM_CAMERA_ID = 61344;
    private static final int PERM_POST_NOTIFICATIONS = 61345;

    private static final int PERM_SHIZUKU_ID = 61346;

    private static final int PERM_WRITE_SECURE_SETTINGS_ID = 61347;

    public static final List<PermissionTypes> CORE_PERMISSIONS = Arrays.asList(
        PermissionTypes.SMS,
        PermissionTypes.CONTACTS,
        PermissionTypes.POST_NOTIFICATIONS
    );

    public static boolean CORE = false;
    public static boolean GPS = false;
    public static boolean DND = false;
    public static boolean DEVICE_ADMIN = false;
    public static boolean OVERLAY = false;
    public static boolean WRITE_SECURE_SETTINGS = false;
    public static boolean NOTIFICATION_ACCESS = false;
    public static boolean CAMERA = false;
    public static boolean BATTERY_OPTIMIZATION = false;

    public static int ENABLED_PERMISSIONS = 0;
    public static final int AVAILABLE_PERMISSIONS = 9;

    public static void initValues(Context context) {
        CORE = checkPermissions(context, CORE_PERMISSIONS);
        GPS = checkPermissions(context, List.of(
            PermissionTypes.FOREGROUND_LOCATION,
            PermissionTypes.BACKGROUND_LOCATION));
        DND = PermissionTypes.DO_NOT_DISTURB.isGranted(context);
        DEVICE_ADMIN = PermissionTypes.DEVICE_ADMIN.isGranted(context);
        OVERLAY = PermissionTypes.OVERLAY.isGranted(context);
        WRITE_SECURE_SETTINGS = PermissionTypes.WRITE_SECURE_SETTINGS.isGranted(context);
        NOTIFICATION_ACCESS = PermissionTypes.READ_NOTIFICATIONS.isGranted(context);
        CAMERA = PermissionTypes.CAMERA.isGranted(context);
        BATTERY_OPTIMIZATION = PermissionTypes.BATTERY_OPTIMIZATIONS.isGranted(context);

        int enabledPermissionSetsCounter = 0;
        for (boolean isPermissionSetEnabled : List.of(
            CORE, GPS, DND, DEVICE_ADMIN, OVERLAY,
            WRITE_SECURE_SETTINGS, NOTIFICATION_ACCESS,
            CAMERA, BATTERY_OPTIMIZATION)) {
            enabledPermissionSetsCounter += isPermissionSetEnabled ? 1 : 0;
        }
        ENABLED_PERMISSIONS = enabledPermissionSetsCounter;
    }

    public static boolean checkPermissions(Context context, Collection<PermissionTypes> types) {
        return types.stream().allMatch(t -> t.isGranted(context));
    }

    public static boolean isShizukuRunning() {
        return Shizuku.pingBinder();
    }

    public static boolean checkShizukuPermission() {
        return Shizuku.checkSelfPermission() == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestShizukuPermission() {
        Shizuku.requestPermission(PERM_SHIZUKU_ID);
    }

    public static void requestWriteSecureSettingsPermissionViaShizuku(Context c) {
        if (!Permission.checkShizukuPermission())
            Permission.requestShizukuPermission();
    }

    public static void requestWriteSecureSettingsPermissionViaRoot(Context c) {
        if (RootAccess.isRooted()) {
            String command = "pm grant " + c.getPackageName() + " android.permission.WRITE_SECURE_SETTINGS";
            RootAccess.execCommand(c, command);
        } else {
            Toast.makeText(c, c.getString(R.string.RootAccessDenied), Toast.LENGTH_LONG).show();
        }
    }
}
