package de.nulide.findmydevice.ui.helper;

public enum SettingsEntryTypes {
    FMD_CONFIG,
    FMD_SERVER,
    ALLOWED_CONTACTS,
    OPENCELLID,
    PERMISSIONS,
    EXPORT_SETTINGS,
    IMPORT_SETTINGS,
    LOGS,
    ABOUT
}
