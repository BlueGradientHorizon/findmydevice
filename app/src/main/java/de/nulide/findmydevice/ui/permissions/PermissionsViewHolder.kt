package de.nulide.findmydevice.ui.permissions

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat.getString
import androidx.recyclerview.widget.RecyclerView
import de.nulide.findmydevice.R
import de.nulide.findmydevice.utils.Permission.PermissionTypes

class PermissionsViewHolder(
    itemView: View,
    private val onGivePermissionClicked: (PermissionTypes) -> Unit,
    private val onGivePermissionViaRootClicked: ((PermissionTypes) -> Unit)?,
    private val onGivePermissionViaShizukuClicked: ((PermissionTypes) -> Unit)?,
    private val onWikiButtonClicked: ((PermissionTypes) -> Unit)?
): RecyclerView.ViewHolder(itemView) {

    fun bind(item: PermissionCardItem) {
        itemView.findViewById<TextView>(R.id.permission_name).text = getString(itemView.context, item.permissionNameStringId)
        itemView.findViewById<TextView>(R.id.permission_description).text = getString(itemView.context, item.permissionDescriptionStringId)

        val buttonGivePermission = itemView.findViewById<Button>(R.id.buttonGivePermission)
        val buttonPermViaRoot = itemView.findViewById<Button>(R.id.buttonPermViaRoot)
        val buttonPermViaShizuku = itemView.findViewById<Button>(R.id.buttonPermViaShizuku)
        val buttonWiki = itemView.findViewById<Button>(R.id.buttonWiki)

        buttonGivePermission.visibility = item.buttonGivePermissionSettings.visibility
        buttonPermViaRoot.visibility = item.buttonPermViaRootSettings.visibility
        buttonPermViaShizuku.visibility = item.buttonPermViaShizukuSettings.visibility
        buttonWiki.visibility = item.buttonWikiSettings.visibility

        buttonGivePermission.isEnabled = item.buttonGivePermissionSettings.enabled
        buttonPermViaRoot.isEnabled = item.buttonPermViaRootSettings.enabled
        buttonPermViaShizuku.isEnabled = item.buttonPermViaShizukuSettings.enabled
        buttonWiki.isEnabled = item.buttonWikiSettings.enabled

        buttonGivePermission.setOnClickListener { onGivePermissionClicked(item.permissionType) }
        buttonPermViaRoot.setOnClickListener { onGivePermissionViaRootClicked?.let { i -> i(item.permissionType) } }
        buttonPermViaShizuku.setOnClickListener { onGivePermissionViaShizukuClicked?.let { i -> i(item.permissionType) } }
        buttonWiki.setOnClickListener { onWikiButtonClicked?.let { i -> i(item.permissionType) } }

        buttonGivePermission.setBackgroundColor(itemView.context.getColor(item.buttonColorId))
    }
}