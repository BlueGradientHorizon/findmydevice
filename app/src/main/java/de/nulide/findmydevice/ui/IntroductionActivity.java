package de.nulide.findmydevice.ui;

import static de.nulide.findmydevice.ui.settings.PermissionsActivity.CORE_PERMISSIONS_REQUIRED_MODE_EXTRA;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import de.nulide.findmydevice.R;
import de.nulide.findmydevice.ui.settings.PermissionsActivity;

public class IntroductionActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);

        findViewById(R.id.buttonNext).setOnClickListener(v ->
            startActivity(
                new Intent(this, PermissionsActivity.class)
                    .putExtra(
                        CORE_PERMISSIONS_REQUIRED_MODE_EXTRA,
                        getIntent().getBooleanExtra(CORE_PERMISSIONS_REQUIRED_MODE_EXTRA, false)
                    )
            )
        );
    }
}
