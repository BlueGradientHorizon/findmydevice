package de.nulide.findmydevice.ui.permissions

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import de.nulide.findmydevice.R
import de.nulide.findmydevice.utils.Permission.PermissionTypes

class PermissionCardsAdapter(
    context: Context,
    private val onGivePermissionClicked: (PermissionTypes) -> Unit,
    private val onGivePermissionViaRootClicked: ((PermissionTypes) -> Unit)?,
    private val onGivePermissionViaShizukuClicked: ((PermissionTypes) -> Unit)?,
    private val onWikiButtonClicked: ((PermissionTypes) -> Unit)?
) : BaseAdapter() {
    var permissionCardsList: List<PermissionCardItem> = listOf(
        PermissionCardItem(
            permissionType = PermissionTypes.SMS,
            permissionNameStringId = R.string.perm_sms_name,
            permissionDescriptionStringId = R.string.Permission_SMS,
            buttonColorId = R.color.colorDisabled
        ),
        PermissionCardItem(
            permissionType = PermissionTypes.CONTACTS,
            permissionNameStringId = R.string.perm_contacts_name,
            permissionDescriptionStringId = R.string.Permission_CONTACTS,
            buttonColorId = R.color.colorDisabled
        ),
        PermissionCardItem(
            permissionType = PermissionTypes.FOREGROUND_LOCATION,
            permissionNameStringId = R.string.perm_location_name,
            permissionDescriptionStringId = R.string.Permission_GPS,
            buttonColorId = R.color.colorEnabled
        ),
        PermissionCardItem(
            permissionType = PermissionTypes.DO_NOT_DISTURB,
            permissionNameStringId = R.string.perm_do_not_disturb_access_name,
            permissionDescriptionStringId = R.string.Permission_DND,
            buttonColorId = R.color.colorEnabled
        ),
        PermissionCardItem(
            permissionType = PermissionTypes.DEVICE_ADMIN,
            permissionNameStringId = R.string.perm_device_admin_name,
            permissionDescriptionStringId = R.string.Permission_DEVICE_ADMIN,
            buttonColorId = R.color.colorEnabled
        ),
        PermissionCardItem(
            permissionType = PermissionTypes.OVERLAY,
            permissionNameStringId = R.string.perm_overlay_name,
            permissionDescriptionStringId = R.string.Permission_OVERLAY,
            buttonColorId = R.color.colorEnabled
        ),
        PermissionCardItem(
            permissionType = PermissionTypes.WRITE_SECURE_SETTINGS,
            permissionNameStringId = R.string.perm_write_secure_settings_name,
            permissionDescriptionStringId = R.string.Permission_WRITE_SECURE_SETTINGS,
            buttonColorId = R.color.colorEnabled
        )   .setButtonGivePermission(View.GONE)
            .setButtonPermViaRoot(View.VISIBLE)
            .setButtonPermViaShizuku(View.VISIBLE)
            .setButtonWiki(View.VISIBLE, true),
        PermissionCardItem(
            permissionType = PermissionTypes.READ_NOTIFICATIONS,
            permissionNameStringId = R.string.perm_notification_access_name,
            permissionDescriptionStringId = R.string.Permission_Notification,
            buttonColorId = R.color.colorEnabled
        ),
        PermissionCardItem(
            permissionType = PermissionTypes.CAMERA,
            permissionNameStringId = R.string.perm_camera_name,
            permissionDescriptionStringId = R.string.Permission_Camera,
            buttonColorId = R.color.colorEnabled
        ),
        PermissionCardItem(
            permissionType = PermissionTypes.BATTERY_OPTIMIZATIONS,
            permissionNameStringId = R.string.perm_battery_optimizations_name,
            permissionDescriptionStringId = R.string.Permission_IGNORE_BATTERY_OPTIMIZATION,
            buttonColorId = R.color.colorEnabled
        ),
        PermissionCardItem(
            permissionType = PermissionTypes.POST_NOTIFICATIONS,
            permissionNameStringId = R.string.perm_post_notification_name,
            permissionDescriptionStringId = R.string.Permission_POST_NOTIFICATIONS,
            buttonColorId = R.color.colorDisabled
        )
    )

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int = permissionCardsList.size

    override fun getItem(position: Int): Any = permissionCardsList[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: PermissionsViewHolder
        if (convertView == null) {
            view = this.inflater.inflate(R.layout.item_permission_card, parent, false)
            vh = PermissionsViewHolder(
                view,
                onGivePermissionClicked,
                onGivePermissionViaRootClicked,
                onGivePermissionViaShizukuClicked,
                onWikiButtonClicked)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as PermissionsViewHolder
        }

        vh.bind(permissionCardsList[position])
        return view
    }
}