package de.nulide.findmydevice.ui.helper;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

public class SettingsEntry {
    public final SettingsEntryTypes type;
    final int stringId;
    final int iconId;

    SettingsEntry(SettingsEntryTypes type, @StringRes int stringId, @DrawableRes int iconId) {
        this.type = type;
        this.stringId = stringId;
        this.iconId = iconId;
    }
}
