package de.nulide.findmydevice.ui.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import de.nulide.findmydevice.R;

public class SettingsViewAdapter extends BaseAdapter {
    public static final List<SettingsEntry> settingsEntriesList = List.of(
        new SettingsEntry(
            SettingsEntryTypes.FMD_CONFIG,
            R.string.Settings_FMDConfig,
            R.drawable.ic_settings),
        new SettingsEntry(
            SettingsEntryTypes.FMD_SERVER,
            R.string.Settings_FMDServer,
            R.drawable.ic_cloud),
        new SettingsEntry(
            SettingsEntryTypes.ALLOWED_CONTACTS,
            R.string.Settings_WhiteList,
            R.drawable.ic_people),
        new SettingsEntry(
            SettingsEntryTypes.OPENCELLID,
            R.string.Settings_OpenCellId,
            R.drawable.ic_cell_tower),
        new SettingsEntry(
            SettingsEntryTypes.PERMISSIONS,
            R.string.Settings_Permissions,
            R.drawable.ic_security),
        new SettingsEntry(
            SettingsEntryTypes.EXPORT_SETTINGS,
            R.string.Settings_Export,
            R.drawable.ic_import_export),
        new SettingsEntry(
            SettingsEntryTypes.IMPORT_SETTINGS,
            R.string.Settings_Import,
            R.drawable.ic_import_export),
        new SettingsEntry(
            SettingsEntryTypes.LOGS,
            R.string.Settings_Logs,
            R.drawable.ic_logs),
        new SettingsEntry(
            SettingsEntryTypes.ABOUT,
            R.string.Settings_About,
            R.drawable.ic_info)
    );

    private final LayoutInflater inflater;

    public SettingsViewAdapter(Context context) {
        inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return settingsEntriesList.size();
    }

    @Override
    public Object getItem(int position) {
        return settingsEntriesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        SettingsEntryViewHolder vh;
        if (convertView == null) {
            view = inflater.inflate(R.layout.item_settings, parent, false);
            vh = new SettingsEntryViewHolder(view);
            view.setTag(vh);
        }
        else {
            view = convertView;
            vh = (SettingsEntryViewHolder) view.getTag();
        }

        vh.bind(settingsEntriesList.get(position));
        return view;
    }
}
