package de.nulide.findmydevice.ui.permissions

import android.view.View
import de.nulide.findmydevice.utils.Permission.PermissionTypes

data class ButtonSettings(
    var visibility: Int,
    var enabled: Boolean
)

data class PermissionCardItem(
    var permissionType: PermissionTypes,
    var permissionNameStringId: Int,
    var permissionDescriptionStringId: Int,

    var buttonGivePermissionSettings: ButtonSettings = ButtonSettings(View.VISIBLE, false),
    var buttonPermViaRootSettings: ButtonSettings = ButtonSettings(View.GONE, false),
    var buttonPermViaShizukuSettings: ButtonSettings = ButtonSettings(View.GONE, false),
    var buttonWikiSettings: ButtonSettings = ButtonSettings(View.GONE, false),

    var buttonColorId: Int
) {
    fun setButtonGivePermission(visibility: Int? = null, enabled: Boolean? = null): PermissionCardItem {
        visibility?.let { this.buttonGivePermissionSettings.visibility = it }
        enabled?.let { this.buttonGivePermissionSettings.enabled = it }
        return this
    }

    fun setButtonPermViaRoot(visibility: Int? = null, enabled: Boolean? = null): PermissionCardItem {
        visibility?.let { this.buttonPermViaRootSettings.visibility = it }
        enabled?.let { this.buttonPermViaRootSettings.enabled = it }
        return this
    }

    fun setButtonPermViaShizuku(visibility: Int? = null, enabled: Boolean? = null): PermissionCardItem {
        visibility?.let { this.buttonPermViaShizukuSettings.visibility = it }
        enabled?.let { this.buttonPermViaShizukuSettings.enabled = it }
        return this
    }

    fun setButtonWiki(visibility: Int? = null, enabled: Boolean? = null): PermissionCardItem {
        visibility?.let { this.buttonWikiSettings.visibility = it }
        enabled?.let { this.buttonWikiSettings.enabled = it }
        return this
    }
}