package de.nulide.findmydevice.ui.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import de.nulide.findmydevice.R;
import de.nulide.findmydevice.data.Settings;
import de.nulide.findmydevice.data.SettingsRepoSpec;
import de.nulide.findmydevice.data.SettingsRepository;
import de.nulide.findmydevice.ui.MainActivity;
import de.nulide.findmydevice.ui.permissions.PermissionCardItem;
import de.nulide.findmydevice.ui.permissions.PermissionCardsAdapter;
import de.nulide.findmydevice.utils.Permission;
import de.nulide.findmydevice.utils.Permission.PermissionTypes;
import kotlin.Unit;

public class PermissionsActivity extends AppCompatActivity {
    public static final String CORE_PERMISSIONS_REQUIRED_MODE_EXTRA = "CORE_PERMISSIONS_REQUIRED_MODE";

    private Button buttonNext;

    private PermissionCardsAdapter permissionCardsAdapter;
    private Settings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        TextView textViewInfoText = findViewById(R.id.textViewInfoText);

        buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(this::onButtonNextClick);

        if (getIntent().getBooleanExtra(CORE_PERMISSIONS_REQUIRED_MODE_EXTRA, false)) {
            textViewInfoText.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.VISIBLE);
        }
        else {
            textViewInfoText.setVisibility(View.GONE);
            buttonNext.setVisibility(View.GONE);
        }

        settings = SettingsRepository.Companion.getInstance(new SettingsRepoSpec(this)).getSettings();
        permissionCardsAdapter = new PermissionCardsAdapter(
            this,
            this::onGivePermissionClicked,
            this::onGivePermissionViaRootClicked,
            this::onGivePermissionViaShizukuClicked,
            this::onWikiButtonClicked);

        ListView permissionCards = findViewById(R.id.permission_cards);
        permissionCards.setAdapter(permissionCardsAdapter);

        updateScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateScreen();
    }

    private void updateScreen() {
        if (Permission.checkPermissions(this, Permission.CORE_PERMISSIONS)) {
            buttonNext.setEnabled(true);
            buttonNext.setBackgroundColor(getColor(R.color.colorEnabled));
        }
        else {
            buttonNext.setEnabled(false);
            buttonNext.setBackgroundColor(getColor(R.color.colorDisabled));
        }

        for (PermissionCardItem permCard: permissionCardsAdapter.getPermissionCardsList()) {
            boolean permissionGranted;

            /*  After the user grants ACCESS_FINE_LOCATION (or it was already granted),
                we replace permission type to BACKGROUND_PERMISSION, so next time he clicks
                on the same button we send him to settings. If you can implement more elegant
                solution, PR is welcome :D
            */
            if (permCard.getPermissionType() == PermissionTypes.FOREGROUND_LOCATION &&
                    PermissionTypes.FOREGROUND_LOCATION.isGranted(this)) {
                permCard.setPermissionType(PermissionTypes.BACKGROUND_LOCATION);
                permissionGranted = PermissionTypes.BACKGROUND_LOCATION.isGranted(this);
            }
            else
                permissionGranted = permCard.getPermissionType().isGranted(this);

            permCard.setButtonGivePermission(null, !permissionGranted);
            permCard.setButtonPermViaRoot(null, !permissionGranted);
            permCard.setButtonPermViaShizuku(null, !permissionGranted && Permission.isShizukuRunning());
        }
        permissionCardsAdapter.notifyDataSetChanged();
    }

    private Unit onGivePermissionClicked(PermissionTypes type) {
        type.request(this);
        return null;
    }

    private Unit onGivePermissionViaRootClicked(PermissionTypes type) {
        if (type == PermissionTypes.WRITE_SECURE_SETTINGS) {
            Permission.requestWriteSecureSettingsPermissionViaRoot(this);
        }
        return null;
    }

    private Unit onGivePermissionViaShizukuClicked(PermissionTypes type) {
        if (type == PermissionTypes.WRITE_SECURE_SETTINGS) {
            Permission.requestWriteSecureSettingsPermissionViaShizuku(this);
        }
        return null;
    }

    private Unit onWikiButtonClicked(PermissionTypes type) {
        if (type == PermissionTypes.WRITE_SECURE_SETTINGS) {
            startActivity(
                new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/Nulide/findmydevice/-/wikis/PERMISSION-WRITE_SECURE_SETTINGS")));
        }
        return null;
    }

    private void onButtonNextClick(View v) {
        settings.setIntroductionPassed();
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }
}
