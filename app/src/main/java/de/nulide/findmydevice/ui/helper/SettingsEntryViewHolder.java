package de.nulide.findmydevice.ui.helper;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import de.nulide.findmydevice.R;

public class SettingsEntryViewHolder extends RecyclerView.ViewHolder {
    private final View itemView;

    public SettingsEntryViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public void bind(SettingsEntry item) {
        TextView name = itemView.findViewById(R.id.textViewSettingsTitle);
        name.setText(itemView.getContext().getString(item.stringId));

        ImageView icon = itemView.findViewById(R.id.imageViewSettingsIcon);
        icon.setImageDrawable(AppCompatResources.getDrawable(itemView.getContext(), item.iconId));
    }
}
